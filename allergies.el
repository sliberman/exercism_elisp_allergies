;;; allergies.el --- Allergies Exercise (exercism)

;;; Commentary:

;;; Code:

(defun allergen-list
    (score)
  "Determine which allergens the given score corresponds to"
  (let ((res ())
	(i 0)
	(allergies '("eggs" "peanuts" "shellfish" "strawberries" "tomatoes" "chocolate" "pollen" "cats")))
    (progn
      (while (not (eq i 8))
	(let ((val (logand 1 (lsh (logand score (expt 2 i)) (* i -1)))))
	  (progn
	    (if (= val 1)
		(add-to-list 'res (nth i allergies)))
	    (setq i (+ i 1)))))
      (reverse res))))

(defun allergic-to-p
    (score allergenic)
  "Return whether a score is associated to an allergenic or not"
  (let ((real-allergens (allergen-list score)))
    (member allergenic real-allergens)))

(provide 'allergies)
;;; allergies.el ends here
